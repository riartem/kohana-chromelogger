## Описание
Kohana ChromePHP - это модуль для Kohana созданный из PHP-библиотеки для Chrome-расширения "Chrome Logger"
Данная библиотека позволяет выводить backend-данные в консоль браузера.

## Требования
- PHP 5 или новее

## Установка
1. Установите расширение Chrome отсюда: https://chrome.google.com/extensions/detail/noaneddfkdjfnfdakjjmocngnfkfehhd
2. Кликните в браузере на иконку расширения для включения плагина в текущей вкладке
3. Подключите модуль, как обычно в Kohana (копируем папку chromelogger в папку modules, прописываем модуль в bootstrap.php: 'chromelogger' => MODPATH.'chromelogger',)
4. Профит!

    ```php
    ChromePhp::log('Hello console!');
    ChromePhp::log($_SERVER);
    ChromePhp::warn('something went wrong!');
    ```

Официальный сайт расширения:
http://www.chromelogger.com